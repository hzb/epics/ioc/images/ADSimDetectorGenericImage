FROM registry.hzdr.de/hzb/epics/base/ubuntu_22_04:1.0.0 AS base

RUN mkdir -p /opt/epics/support

RUN apt update
# RUN apt install -y libtiff5 libjpeg-turbo8 libxml2 libxml2-dev libzstd1 libaravis-0.8-0 libaravis-dev
RUN apt install -y libtiff5\
    libjpeg-turbo8 libzstd1\
    libaravis-0.8-0 libaravis-dev\
    libx11-dev libxcomposite-dev\
    libboost1.74-dev libboost-test-dev\
    libboost-system-dev

## Install required EPICS modules and areaDetector

# configure environment
COPY configure/epics/RELEASE.local ${SUPPORT}/RELEASE.local

# install autosave 
RUN git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
RUN make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
RUN git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
RUN make -C ${SUPPORT}/seq -j $(nproc)

# install sscan
RUN git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
RUN make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
RUN git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
RUN make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
RUN apt install -y libntirpc-dev libtirpc3
RUN git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
RUN sed -i '/TIRPC/s/^#//g' ${SUPPORT}/asyn/configure/CONFIG_SITE
RUN make -C ${SUPPORT}/asyn -j $(nproc)

# install busy
RUN git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy
RUN make -C ${SUPPORT}/busy -j $(nproc)

# install iocStats
RUN git clone --depth 1 --recursive --branch 3.2.0 https://github.com/epics-modules/iocStats.git ${SUPPORT}/iocStats
RUN make -C ${SUPPORT}/iocStats -j $(nproc)

## AreaDetector setup
# clone areaDetector main repo
RUN git clone --branch R3-12-1 https://github.com/areaDetector/areaDetector.git ${SUPPORT}/areaDetector
RUN git clone --branch R3-12-1 https://github.com/areaDetector/ADCore.git ${SUPPORT}/areaDetector/ADCore
RUN git clone --branch R1-10 https://github.com/areaDetector/ADSupport.git ${SUPPORT}/areaDetector/ADSupport

# configure areaDetector
RUN cd ${SUPPORT}/areaDetector/configure &&\
    cp EXAMPLE_RELEASE.local RELEASE.local &&\
    cp EXAMPLE_RELEASE_LIBS.local RELEASE_LIBS.local &&\
    cp EXAMPLE_CONFIG_SITE.local CONFIG_SITE.local &&\
    cp EXAMPLE_CONFIG_SITE.local.linux-x86_64 CONFIG_SITE.local.linux-x86_64

# configure areaDetector environment
COPY configure/areaDetector/RELEASE_PRODS.local ${SUPPORT}/areaDetector/configure/RELEASE_PRODS.local
COPY configure/areaDetector/RELEASE_LIBS.local ${SUPPORT}/areaDetector/configure/RELEASE_LIBS.local

# clone ADSimDetector
RUN git clone https://github.com/areaDetector/ADSimDetector.git ${SUPPORT}/areaDetector/ADSimDetector
# last working commit of ADSimDetector together with the areadtector R3-12-1 release
RUN git -C ${SUPPORT}/areaDetector/ADSimDetector reset --hard 14750dd

# include ADSimDetector to RELEASE
RUN sed -i '/ADSIMDETECTOR/s/^#//g' ${SUPPORT}/areaDetector/configure/RELEASE.local

# install areaDetector
RUN make -C ${SUPPORT}/areaDetector -j $(nproc)

# set up envPaths and startup file
RUN cd ${SUPPORT}/areaDetector/ADSimDetector/iocs/simDetectorIOC/iocBoot/iocSimDetector &&\
    cp envPaths envPaths.linux
