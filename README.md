# Docker image for areaDetector

A Docker image for `areaDetector` support module.

The image contains only `ADCore` and `ADSupport` components.

## Configuration

There are 3 files to define the configuration: `RELEASE.local` is common configuration for the EPICS modules, while `RELEASE_LIBS.local` and `RELEASE_PRODS.local` are specific to the `areaDetector` framework.

test
